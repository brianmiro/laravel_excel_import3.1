<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class Usersimport implements ToModel, WithValidation, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'name' => $row['name'],
            'email' => $row['email'],
            'password' =>'',
        ]);
    }
    public function rules(): array
    {
        return [
            'name' =>['string'],
            'email'=>['string'],
        ];
    }
    public function customValidationMessages()
    {
        return [
            'name.numeric' => 'La columna Nombre solo admite valores numericos ',
            'email.string' => 'La columna Email solo admite valores alfanumericos ',

        ];
    }
   /* public function customValidationAttributes()
    {
        return ['1' => 'nombre'];
    }*/
}
