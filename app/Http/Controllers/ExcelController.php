<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Maatwebsite\Excel\Concerns\ToArray;

class ExcelController extends Controller
{
    public function index()
    {
       // $users=User::all();
        return view('import_excel');
    }
    public function import(Request $request){
        $this->validate($request, [
                'import_file'  =>['required','mimes:xls,xlsx']
                 ],['import_file.required'=>'Selecc. un archivo',
                'import_file.mimes'=>'Formato incorrecto,extension valida : xls o xlsx',]
        );

        Excel::import(new UsersImport(),$request->file('import_file'));
        return redirect()->route('home')->with('success', 'Archivo Importado con exito!');;

    }
    public function importToArray(Request $request)
    {
        //validaciones
        $this->validate($request, [
            'import_file'  =>['required','mimes:xls,xlsx']
             ],['import_file.required'=>'Selecc. un archivo',
            'import_file.mimes'=>'Formato incorrecto,extension valida : xls o xlsx',]
        );
        //convertir archivo excel en array
        $archivo = $request->file('import_file');
        $array = (new UsersImport)->toArray($archivo);
        //dd($array[0]);
        //recorrer array 
        foreach($array[0] as $a){
            dd($a);
        }

    }
}
